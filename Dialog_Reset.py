import gi
gi.require_version("Gtk","3.0")   #parchar un error de version
from gi.repository import Gtk

class DlgReset():  #clase que contrla las interacciones de dlg_acept

    def __init__(self,t01,t02,sumsum):  #constructor
        self.builder = Gtk.Builder()  # permite leer el archivo y poder cargarlo dentro de aplicacion
        self.builder.add_from_file("./ui/Main_Glade.ui")  #recibe la ruta donde esta la ventana

        self.dialog = self.builder.get_object("dlg_reset")  #nombre de la pestaña que saldrá
        self.dialog.set_default_size(300, 200)  #tamaño
        
        self.reset_btn_acept = self.builder.get_object("reset_btn_acept")
        self.reset_btn_acept.connect("clicked", self.close)

        self.reset_btn_cancel = self.builder.get_object("reset_btn_cancel")
        self.reset_btn_cancel.connect("clicked", self.close)

        self.dialog.set_markup(f"¿Desea reiniciar la cuenta?")
        self.dialog.format_secondary_text(f"Los textos {t01} y {t02}" 
                                            "serán eliminados"
                                            "incluyendo las sumas")

        self.dialog.show_all()

    def close(self, btn=None):
        self.dialog.destroy()   
