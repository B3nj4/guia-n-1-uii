import gi
gi.require_version("Gtk","3.0")   #parchar un error de version
from gi.repository import Gtk
from Dialog_Acept import DlgAcept
from Dialog_Reset import DlgReset

class MainWindow():  #clase

    def __init__(self):  #constructor
        self.builder = Gtk.Builder()  # permite leer el archivo y poder cargarlo dentro de aplicacion
        self.builder.add_from_file("./ui/Main_Glade.ui")  #recibe la ruta donde esta la ventana

        self.window = self.builder.get_object("Main_Glade") #llamar y manipular archivos --> instancias
        self.window.connect("destroy", Gtk.main_quit) #da señales para poder interactuar con ventana, en este caso destroy hace que al clickear algo lo cierre
        self.window.set_title("Guia 1 - Unidad II")   # Titulo de la ventana
        self.window.resize(800, 600)  #Tamaño de la ventana

        self.text_01 = self.builder.get_object("entry_01") 
        self.text_02 = self.builder.get_object("entry_02")

        self.lbl_01 = self.builder.get_object("lbl_01")
        self.lbl_02 = self.builder.get_object("lbl_02") 

        self.open_acept = self.builder.get_object("button_acept")
        self.open_acept.connect("clicked", self.on_open_glade_acept)

        self.open_reset = self.builder.get_object("button_reset")
        self.open_reset.connect("clicked", self.on_open_glade_reset)

        self.window.show_all()  #muestra la ventana y su contenido

    def on_open_glade_acept(self, btn=None):

        self.sum_string()        
        DlgAcept(self.t01,self.t02,self.sumsum)

    def sum_string(self, btn=None):

        self.t01 = self.text_01.get_text()   #texto = objeto
        self.t02 = self.text_02.get_text()
        
        try:
            tsum01 = int(self.t01)
            tsum02 = int(self.t02) 

        except ValueError:
            tsum01 = len(self.t01)
            tsum02 = len(self.t02)
        
        self.sumsum = tsum01 + tsum02

    def on_open_glade_reset(self, btn=None):

        self.dialog = DlgReset(self.t01,self.t02,self.sumsum,self.reset_btn_cancel,self.reset_btn_acept)
        response_acept = self.reset_btn_acept.dialog.run()
        response_cancel = self.reset_btn_cancel.dialog.run()

        if response_acept == Gtk.ResponseType.OK:
            self.t01.set_text("") 
            self.t02.set_text("")
            #self.sumsum.set.text("")
            self.sumsum = 0
        
        elif response_cancel == Gtk.ResponseType.Cancel:
            self.dialog.destroy()

        DlgReset(self.t01,self.t02,self.sumsum,self.reset_btn_cancel,self.reset_btn_acept)        
    
if __name__ == "__main__":
    MainWindow()
    Gtk.main()   #elemento que muestra toda la interfaz grafica (ventana)
