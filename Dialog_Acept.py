import gi
gi.require_version("Gtk","3.0")   #parchar un error de version
from gi.repository import Gtk

class DlgAcept():  #clase que contrla las interacciones de dlg_acept

    def __init__(self,t01,t02,sumsum):  #constructor
        self.builder = Gtk.Builder()  # permite leer el archivo y poder cargarlo dentro de aplicacion
        self.builder.add_from_file("./ui/Main_Glade.ui")  #recibe la ruta donde esta la ventana

        self.dialog = self.builder.get_object("dlg_acept")  #nombre de la pestaña que saldrá
        self.dialog.set_default_size(300, 200)  #tamaño
       
        self.acept_btn = self.builder.get_object("acept_btn")
        self.acept_btn.connect("clicked", self.close)

        self.dialog.set_markup(f"Textos ingresados son:")
        self.dialog.format_secondary_text(f"{t01} y {t02} los cuales suman: {sumsum}") 
        self.dialog.show_all()
        
    def close(self, btn=None):
        self.dialog.destroy()
